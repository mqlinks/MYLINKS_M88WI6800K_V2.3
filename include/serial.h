/*=============================================================================+
|                                                                              |
| Copyright 2012                                                               |
| Montage Inc. All right reserved.                                             |
|                                                                              |
+=============================================================================*/
#include <mt_types.h>

struct serial_buffer
{
	unsigned short		buffsize;
	unsigned short		space;
	u8		*purbuff;
	u8		*pin;
	u8		*pout;
	//struct serial_buffer *next;
};

/*-----------------------------------------------------------------------------+
| Common Macros                                                                |
+-----------------------------------------------------------------------------*/
#define UART_TX_BUF_SIZE 4096
#define UART_BR_TB       16

/*-----------------------------------------------------------------------------+
| Function Prototypes                                                          |
+-----------------------------------------------------------------------------*/
/** @defgroup serial-api Serial Functions
 *  @{
 */
/**
 * Uart configuration
 * @param br_id baudrate table index 1 ~ 12\n
 * 1: 2400 2: 4800 3: 9600\n
 * 4: 19200 5: 38400 6: 57600\n
 * 7: 115200 8: 230400 9: 460800\n
 * 10: 500000 11: 576000 12: 921600\n
 * 13: 1000000 14: 1152000 15: 1500000
 * @param parity 0: none\n 1: odd\n 2:even
 * @param stopbits 1, 2 bit
 * @param chan uart chan 0 ~ 2
 * @param threshold FIFO threshold for interrupt
 * @return None
 */
void serial_conf(int br_id, int parity, int stopbits, int chan, int threshold);
/**
 * Serial initial
 *
 * chan 0(UART1), chan 1-2(UART2), initial tx buffer for transparent mode.
 * @param chan: uart channel 0 ~ 2
 * @return 1: success\n 0: error
 */
int serial_init(int chan);

/**
 * Uart tx put character no wait
 * @param chan uart channal 0 ~ 2
 * @param    c character data
 * @return 0: tx fifo not full\n -1: tx fifo full
 */
int uart_no_wait_putc(int chan, int c);
/**
 * Uart get character, wait until time out
 * @param chan uart channel 0 ~ 2
 * @return character, -1 means no data available
 */
int uart_timeout_getc(int chan);
/**
 * Set the maximum milliseconds to wait for serial data
 * @param set_timeout time(ms)
 * @return None
 */
void uart_set_timeout(unsigned int set_timeout);
/**
 * Copy data to tx buffer and insert to fifo
 *
 * Need to initial txbuf, call serial_init(chan) first
 * @param    chan uart channel 0 ~ 2
 * @param   pdata rx buffer
 * @param datalen data length
 * @return stat\n
 *  0: done\n
 *  1: busy, tx buffer is full\n
 *  -1: fail, tx buffer is null
 */
int serial_write(int chan, char* pdata, int datalen);
/**
 * Read serial data
 * @param   mode
 *  0: read one byte\n
 *  1: read bytes\n
 *  2: read bytes until terminator character
 * @param   chan uart channel
 * @param   *buf read buffer pointer
 * @param   len data length
 * @param   end_c terminator character
 * @return
 *  mode 0: return the first byte of incoming serial data (-1 means no data available)\n
 *  mode 1, 2: return data length (0 means no valid data was found)
 */
int serial_read_byte(int mode, int chan, char *buf, int len, char end_c);
/*! @} */
struct serial_buffer* serial_buf_init(unsigned int size);
int serial_buffer_enqueue(struct serial_buffer *ptxbuf, char *pdata , int datalen);
char *serial_read_string(int mode, int chan, char end_c);
#ifdef CONFIG_UR_FLOW_CONTROL
void uart_flow_cts_stat(void);
#endif
