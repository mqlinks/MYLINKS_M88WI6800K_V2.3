/*=============================================================================+
|                                                                              |
| Copyright 2012                                                               |
| Montage Inc. All right reserved.                                             |
|                                                                              |
+=============================================================================*/
#ifndef __GPIO_H__
#define __GPIO_H__
#include <arch/trap.h>
/*-----------------------------------------------------------------------------+
| Common Macros                                                                |
+-----------------------------------------------------------------------------*/
#define GPIO_OUT 1
#define GPIO_IN  0
#define HIGH 1
#define LOW  0

typedef void (*gpio_func_cb)( void* arg );

typedef struct gpio_irq_handler
{
	gpio_func_cb hnd;
	void* arg;	
} gpio_irq_handler;

/*-----------------------------------------------------------------------------+
| Function Prototypes                                                          |
+-----------------------------------------------------------------------------*/
/** @defgroup gpio-api GPIO Functions
 *  @{
 */
/**
 * Enable GPIO function.
 * @param pin GPIO number
 * @param mode 0: disable\n 1: enable
 * @return None
 */
void gpio_enable(int pin, int mode);
/**
 * Set GPIO pin mode, include gpio_enable.
 * @param pin GPIO number
 * @param mode 0: input\n 1: output
 * @return None
 */
void pin_mode(int pin, int mode);
/**
 * Read GPIO pin input data, call it after pin_mode.
 * @param pin GPIO number
 * @return 0: low\n 1: high
 */
int digital_read(int pin);
/**
 * Set GPIO pin output data, call it after pin_mode.
 * @param pin GPIO number
 * @param val 0: low\n 1: high
 * @return None
 */
void digital_write(int pin, int val);
/**
 * Set two GPIO pin output data at the same time, call it after pin_mode.
 * @param pin GPIO number
 * @param val 0: low\n 1: high
 * @param pin2 GPIO number
 * @param val2 0: low\n 1: high
 * @return None
 */
void digital_write_two(int pin, int val, int pin2, int val2);
/**
 * Enable GPIO pin interrupt.
 * @param pin GPIO number
 * @param mode 0: rising\n 1: falling\n 2:high level\n 3:low level\n
 * @param handler : a function pointer to the interrupt handler
 * @param arg     : an argument that will be passed to the
 *                  interrupt handler
 * @return None
 */
void pin_en_intr(int pin, int mode, gpio_func_cb handler,void* arg);
/**
 * Disable GPIO pin interrupt.
 * @param pin GPIO number
 * @return None
 */
void pin_dis_intr(int pin);
/*! @} */
void pin_trigger(int pin);
void gpio_handler(void);
#endif
