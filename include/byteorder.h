#ifndef __BYTEORDER_H_
#define __BYTEORDER_H_
#include <mt_types.h>
#include <arch/cpu.h>

/*
 * casts are necessary for constants, because we never know how for sure
 */
#define constant_swab16(x) ((__u16)(				\
	(((__u16)(x) & (__u16)0x00ffU) << 8) |			\
	(((__u16)(x) & (__u16)0xff00U) >> 8)))

#define constant_swab32(x) ((__u32)(				\
	(((__u32)(x) & (__u32)0x000000ffUL) << 24) |		\
	(((__u32)(x) & (__u32)0x0000ff00UL) <<  8) |		\
	(((__u32)(x) & (__u32)0x00ff0000UL) >>  8) |		\
	(((__u32)(x) & (__u32)0xff000000UL) >> 24)))

#define constant_swab64(x) ((__u64)(				\
	(((__u64)(x) & (__u64)0x00000000000000ffULL) << 56) |	\
	(((__u64)(x) & (__u64)0x000000000000ff00ULL) << 40) |	\
	(((__u64)(x) & (__u64)0x0000000000ff0000ULL) << 24) |	\
	(((__u64)(x) & (__u64)0x00000000ff000000ULL) <<  8) |	\
	(((__u64)(x) & (__u64)0x000000ff00000000ULL) >>  8) |	\
	(((__u64)(x) & (__u64)0x0000ff0000000000ULL) >> 24) |	\
	(((__u64)(x) & (__u64)0x00ff000000000000ULL) >> 40) |	\
	(((__u64)(x) & (__u64)0xff00000000000000ULL) >> 56)))

#define constant_swahw32(x) ((__u32)(			\
	(((__u32)(x) & (__u32)0x0000ffffUL) << 16) |		\
	(((__u32)(x) & (__u32)0xffff0000UL) >> 16)))

#define constant_swahb32(x) ((__u32)(			\
	(((__u32)(x) & (__u32)0x00ff00ffUL) << 8) |		\
	(((__u32)(x) & (__u32)0xff00ff00UL) >> 8)))


#ifdef ___swab16
#define __swab16(x) \
	(__builtin_constant_p((__u16)(x)) ? \
	constant_swab16(x) : \
	___swab16(x))
#else
#define __swab16(x) \
	(constant_swab16((__u16)(x))) 
#endif

#ifdef ___swab32
#define __swab32(x) \
	(__builtin_constant_p((__u32)(x)) ? \
	constant_swab32(x) : \
	___swab32(x))
#else
#define __swab32(x) \
	(constant_swab32((__u32)(x))) 
#endif

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
# define cpu_to_be16 __swab16
# define cpu_to_be32 __swab32
# define be16_to_cpu __swab16
# define be32_to_cpu __swab32
# define const_cpu_to_be16 constant_swab16
# define const_cpu_to_be32 constant_swab32
# define const_be16_to_cpu constant_swab16
# define const_be32_to_cpu constant_swab32
# define cpu_to_le16(x) (x)
# define cpu_to_le32(x) (x)
# define le16_to_cpu(x) (x)
# define le32_to_cpu(x) (x)
# define const_cpu_to_le16(x) (x)
# define const_cpu_to_le32(x) (x)
# define const_le16_to_cpu(x) (x)
# define const_le32_to_cpu(x) (x)
#else
# define cpu_to_be16(x) (x)
# define cpu_to_be32(x) (x)
# define be16_to_cpu(x) (x)
# define be32_to_cpu(x) (x)
# define const_cpu_to_be16(x) (x)
# define const_cpu_to_be32(x) (x)
# define const_be16_to_cpu(x) (x)
# define const_be32_to_cpu(x) (x)
# define cpu_to_le16 __swab16
# define cpu_to_le32 __swab32
# define le16_to_cpu __swab16
# define le32_to_cpu __swab32
# define const_cpu_to_le16 constant_swab16
# define const_cpu_to_le32 constant_swab32
# define const_le16_to_cpu constant_swab16
# define const_le32_to_cpu constant_swab32
#endif

#endif

