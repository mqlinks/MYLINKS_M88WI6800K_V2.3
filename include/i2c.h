/*=============================================================================+
|                                                                              |
| Copyright 2012                                                               |
| Montage Inc. All right reserved.                                             |
|                                                                              |
+=============================================================================*/
/*-----------------------------------------------------------------------------+
| Common Macros                                                                |
+-----------------------------------------------------------------------------*/
#if 0
/* only for STA mode, beacuse GPIO 10 is mode selection pin.
   NIC mode(LOW) would make SDA data wrong */
#define SDA 10
#define SCL 19
#else//only for testing
#define SDA 18
#define SCL 17
#endif

enum {
//	GPIO_IN = 0,
//	GPIO_OUT = 1,
	delay_us = 1,
	delay_mid = 2,
	delay_last = 5,
};

/*-----------------------------------------------------------------------------+
| Function Prototypes                                                          |
+-----------------------------------------------------------------------------*/
/** @defgroup i2c-api I2C Functions
 *  @{
 */
/**
 * I2C master send data(1 byte).
 * @param slave_addr I2C slave address
 * @param byte data
 * @return 0: ack
 */
int i2c_send_byte(unsigned char slave_addr, unsigned char byte);
/**
 * I2C master send string.
 * @param slave_addr I2C slave address
 * @param *str data pointer
 * @return 0: ack
 */
int i2c_send_str(unsigned char slave_addr, char *str);
/**
 * I2C master send data.
 * @param slave_addr I2C slave address
 * @param *data data pointer
 * @param len data length
 * @return 0: ack
 */
int i2c_send_data(unsigned char slave_addr, char *data, int len);
/**
 * I2C master read data(1 byte).
 * @param slave_addr I2C slave address
 * @return data
 */
int i2c_read_byte(unsigned char slave_addr);
/**
 * I2C master read data.
 * @param slave_addr I2C slave address
 * @param *str read data pointer
 * @param len data length, master must know the correct length of data
 * @return None
 */
void i2c_read_data(unsigned char slave_addr, char *str, int len);
/*! @} */
