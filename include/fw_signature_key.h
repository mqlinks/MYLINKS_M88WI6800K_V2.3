/*=============================================================================+
|                                                                              |
| Copyright 2012                                                               |
| Montage Inc. All right reserved.                                             |
|                                                                              |
+=============================================================================*/
/*! 
*   \file 
*   \brief  
*   \author Montage
*/

#ifndef _FW_SIGHNATURE_KEY_H_
#define _FW_SIGHNATURE_KEY_H_

#define ECC_KEY_LEN 33

#ifdef CONFIG_FW_SIGN_AESENC	
	#define FW_AESKEY "qwertyuiasdfghjl"
#elif CONFIG_FW_SIGN_ECCENC
	const char fw_com_pubkey[ECC_KEY_LEN] = {0x03,0x57,0xa7,0x44,0xa8,0x69,0xd3,0x8f,0x2c,0xdf,0x63,0x0c,0xba,0x52,0xf3,0xbc,0xdd,0x00,0x01,0x82,0xb2,0xed,0x12,0xfc,0xd6,0xb7,0xcb,0xd4,0x47,0x76,0x14,0x9c,0x0f};
#endif

#endif
