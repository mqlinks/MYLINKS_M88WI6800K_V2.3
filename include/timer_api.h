/*=============================================================================+
|                                                                              |
| Copyright 2014                                                               |
| Montage Inc. All right reserved.                                             |
|                                                                              |
+=============================================================================*/
/*!
*   \file   timer_api.h
*   \brief  define Timer API functions
*   \author Montage
*/

#ifndef TIMER_API_H
#define TIMER_API_H

/** @defgroup timer-api Timer Functions
 *  @{
 */
/**
 * The function register a callback function on software timer list.
 * Once the specific time is up, the call back function will be invoked.
 * @param timer_func Pointer to callback function.
 * @param func_parm Parameter of callback function.
 * @param msec Milliseconds to count down.
 * @return None.
 */
void* add_timeout(void (*timer_func)(void *), void *func_parm, unsigned int msec);
/**
 * Random seed generator.
 * @return A random seed.
 */
int arc4random(void);
/**
 * The function is used to delete a previously timer, registered on
 * software timer. Note that the timer_func and func_parm must have
 * same value with add_timeout().
 * @param timer_func Pointer to callback function.
 * @param func_parm Parameter of callback function.
 * @return None.
 */
void del_timeout(void (*timer_func)(void *), void *func_parm);
/**
 * Generate random byte in specific array.
 * @param buf Pointer to input array.
 * @param len Array size.
 * @return None.
 */
void get_random_bytes(void *buf, unsigned int len);
/**
 * Start hardware timer1 to countdown. When time is up, callback
 * function is invoked and the timer is reload according auto load
 * flag.
 * @param us Microsecond to timeout.
 * @param func Pointer to callback function.
 * @param autoload Autoload flag.
 * @return None.
 */
void hw_timer_start(unsigned int us, void (*func)(void), int autoload);
/**
 * Stop hardware timer
 * @return None.
 */
void hw_timer_stop(void);
/**
 * Get system time in microsecond.
 * @return Microsecond.
 */
unsigned int micros(void);
/**
 * Get system time in millisecond.
 * @return Millisecond.
 */
unsigned int millis(void);
/**
 * Delay specific microseconds.
 * @param us Microseconds to wait.
 * @return None.
 */
void udelay(unsigned int us);
/*! @} */
#endif
