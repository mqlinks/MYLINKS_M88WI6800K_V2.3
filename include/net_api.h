/*=============================================================================+
|                                                                              |
| Copyright 2014                                                               |
| Montage Inc. All right reserved.                                             |
|                                                                              |
+=============================================================================*/
/*!
*   \file   net_api.h
*   \brief  define net API functions
*   \author Montage
*/

#ifndef NET_API_H
#define NET_API_H

/** @defgroup net-api Netowrk Functions
 *  @{
 */
/**
 * The function ping destination address.
 * @param dip Destination address.
 * @param size Packet size.
 * @param iter Iteration.
 * @param to Timeout(seconds).
 * @param interval Packet interval(milliseconds).
 * @return None.
 */
void net_ping(unsigned int dip, unsigned int *size,
				unsigned int *iter, unsigned int *to,
				unsigned int *interval);
/**
 * The function sets DNS server by server index.
 * @param idx The DNS server index.
 * @param ipaddr IP address.
 * @return None.
 */
void net_set_dns(int idx, unsigned int *ipaddr);
/**
 * The function gets DNS server's IP address.
 * @param idx The DNS server index.
 * @param ipaddr IP address.
 * @return IP string of DNS server.
 */
char *net_get_dns(int idx, unsigned int *ipaddr);
/**
 * The function gets netif name.
 * @param idx The netif index.
 * @return Name string of netif.
 */
char *net_get_name(int idx);
/**
 * The function queries host IP address by hostname. It also dumps
 * host's IP address.
 * @param name The hostname.
 * @param ipaddr The host IP address.
 * @return 1: succeed\n 0: failed
 */
int net_get_hostbyname(const char *name, char *ipaddr);
/**
 * The function gets client information.
 * @param mac The client's address.
 * @return err_t error code
 */
int net_get_client_info(const char *mac);
/**
 * The function reads current IP status on a network interface.
 * @param data: Point to the buffer to store the IP address.
 * @param type: Specifies wlan interface.
 * @return None.
 */
void net_if_ip_sts(void *data, int type);
/**
 * The function initials LWIP device and all of netifs name.
 * @return None.
 */
void net_drv_init(void);
/**
 * The function initializes notification center.
 * @return NO_ERR.
 */
int net_init_notification(void);
/**
 * The function register notification and it's callback function.
 * @param type system defined notifications.
 * @param functionAddress callback function.
 * @return NO_ERR.
 */
int net_add_notification(int type, void *functionAddress);
/**
 * The function unregister notification and it's callback function.
 * @param type system defined notifications.
 * @return NO_ERR.
 */
int net_del_notification(int type);
/**
 * The function unregister all notification and callback functions.
 * @param type system defined notifications.
 * @return NO_ERR.
 */
int net_del_notification_all(int type);
/**
 * The function sets interface up and network configurations.
 * @param idx The netif index.
 * @param dhcp DHCP mode.
 * @param mac MAC address.
 * @param _ip IP address.
 * @param _mask Net mask.
 * @param _gw Gateway address.
 * @param _dns DNS server address.
 * @return None.
 */
void net_if_up(int idx, unsigned char dhcp, char *mac,
				unsigned char *_ip, unsigned char *_mask,
				unsigned char *_gw, unsigned char *_dns);
/**
 * The function sets interface down.
 * @param idx The netif index.
 * @return None.
 */
void net_if_down(int idx);
/*! @} */
#endif
