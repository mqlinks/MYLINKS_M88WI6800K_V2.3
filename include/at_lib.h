#ifndef __AT_LIB_H
#define __AT_LIB_H

typedef struct
{
	char *at_cmdName;
	char (*at_setupCmd)(unsigned char id,int argc,char *argv[]);
	char (*at_exeCmd)(unsigned char id);
}at_fun_type;

#endif
