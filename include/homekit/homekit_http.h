#ifndef HOMEKIT_HTTP_H_
#define HOMEKIT_HTTP_H_

#include <stdbool.h>
bool save_data(char *precv, uint16_t length);
bool check_data(char *precv, uint16_t length);


void acc_send(void *arg);
void data_send(void *arg, bool responseOK, char *psend);
void response_send(void *arg, bool responseOK);
void event_send(void *arg, char *psend);
void h204_send(void *arg);
void tlv8_parse(char *pbuf, uint16_t len, char *objects[], int objects_len[]);
void tlv8_add(char *pbuf, uint16_t *index, int type, uint16_t len, char *value);
void tlv8_send(void *arg, char *pbuf, uint16_t len);
void tlv8_close(char *pbuf, uint16_t *index);
void parse_url(char *precv, URL_Frame *purl_frame);

void decrypt(void *arg, char *data, unsigned short *length);
void encrypt(void *arg, char *data, unsigned short *length);

#endif
