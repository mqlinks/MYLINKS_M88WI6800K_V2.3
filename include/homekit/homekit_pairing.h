#ifndef HOMEKIT_PAIRING_H_
#define HOMEKIT_PAIRING_H_

#include <lwip/tcp.h>
#include "wolfssl/wolfcrypt/types.h"
#include "wolfssl/wolfcrypt/chacha20_poly1305.h"
#include "homekit/homekit_pairing.h"

//#define DEMO
#define FACTORY
#define DEBUG0
#define DEBUG1
#define DEBUG2
#define DEBUG3
#define DEBUG4
//#define DEBUG5
//#define DEBUG6
//#define DEBUG7
//#define DEBUG8
//#define DEBUG9

//#define DEBUG_FLAG 0
#define TLVNUM 12

typedef struct _crypto_parm {
	xSemaphoreHandle semaphore;
    struct tcp_pcb *pcb;
    int             state;
    int             stale;
    uint32_t        connectionid;
    int             encrypted;
    long            countwr;
    long            countrd;
    word32          sessionkey_len;
    byte            sessionkey[32];
    byte            verKey[CHACHA20_POLY1305_AEAD_KEYSIZE];
    byte            readKey[CHACHA20_POLY1305_AEAD_KEYSIZE];
    byte            writeKey[CHACHA20_POLY1305_AEAD_KEYSIZE];
    char            object[0x1cb];
    int             objects_len[TLVNUM];
} crypto_parm;


#define PASSWORD "121-00-121"
#define PASSWORD_LEN 10

void crypto_init(void);
void crypto_tasks(void *arg);
void homekit_crypto_init(void);

//void handle_request(void *arg, URL_Frame *pURL_Frame, uint8_t* payload, uint16_t p_length);
void handle_request(void *arg, uint8_t* payload, uint16_t p_length);
void pairing_setup_handle_M1(void *arg);
void pairing_setup_handle_M3(void *arg);
void pairing_setup_handle_M5(void *arg);
void pairing_verify_handle_M1(void *arg);
void pairing_verify_handle_M3(void *arg);
void pairing_add(void *arg);
void pairing_del(void *arg);
void send_error(struct tcp_pcb *pcb, uint8_t error_code, uint8_t pairing_state);

#endif /* HOMEKIT_PAIRING_H_ */
