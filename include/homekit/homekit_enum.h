#ifndef HOMEKIT_ENUM_H_
#define HOMEKIT_ENUM_H_

enum {
    PAIR_M1 = 0x01,
    PAIR_M2 = 0x02,
    PAIR_M3 = 0x03,
    PAIR_M4 = 0x04,
    PAIR_M5 = 0x05,
    PAIR_M6 = 0x06,
    PAIR_M7 = 0x07,
} Pair_State; 

enum {
	PAIR_SETUP = 0x01,
	PAIR_VERIFY = 0x02,
	ADD_PAIRING = 0x03,
	REMOVE_PAIRING = 0x04,
	LIST_PAIRING = 0x05
} Methods;

enum {
	kTLVError_Unknown = 0x01,
	kTLVError_Authentication = 0x02,
	kTLVError_Backoff = 0x03,
	kTLVError_MaxPeers = 0x04,
	kTLVError_MacTries = 0x05,
	kTLVError_Unavilable = 0x06,
	kTLVError_Busy = 0x07
} Homekit_Error;

enum {
    TYPE_METHOD = 0x00,
    TYPE_IDENTIFIER = 0x01,
    TYPE_SALT = 0x02,
    TYPE_PUBLIC_KEY = 0x03,
    TYPE_PROOF = 0X04,
    TYPE_ENCRYPTED_DATA = 0x05,
    TYPE_STATE = 0x06,
    TYPE_ERROR = 0X07,
    TYPE_RETRYDELAY = 0x08,
    TYPE_CERTIFICATE = 0x09,
    TYPE_SIGNATURE = 0x0A,
    TYPE_PERMISSONS = 0x0B,
    TYPE_FRAGMENTDATA = 0x0C,
    TYPE_FRAGMENTLAST = 0x0D,
    TYPE_SEPARATOR = 0xFF
} Tags; 

#endif /* HOMEKIT_ENUM_H_ */