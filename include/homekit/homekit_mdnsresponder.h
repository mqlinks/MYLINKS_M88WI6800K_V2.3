#ifndef __MDNSRESPONDER_H__
#define __MDNSRESPONDER_H__

#include <mt_types.h>
#include <lwip/sockets.h>

/*
 * Basic multicast DNS responder
 * 
 * Advertises the IP address, port, and characteristics of a service to other devices using multicast DNS on the same LAN,
 * so they can find devices with addresses dynamically allocated by DHCP. See avahi, Bonjour, etc
 * See RFC6762, RFC6763
 *
 * This sample code is in the public domain.
 *
 * by M J A Hamel 2016
 */

/** DNS field TYPE used for "Resource Records" */
#define DNS_RRTYPE_A              1     /* a host address */
#define DNS_RRTYPE_NS             2     /* an authoritative name server */
#define DNS_RRTYPE_MD             3     /* a mail destination (Obsolete - use MX) */
#define DNS_RRTYPE_MF             4     /* a mail forwarder (Obsolete - use MX) */
#define DNS_RRTYPE_CNAME          5     /* the canonical name for an alias */
#define DNS_RRTYPE_SOA            6     /* marks the start of a zone of authority */
#define DNS_RRTYPE_MB             7     /* a mailbox domain name (EXPERIMENTAL) */
#define DNS_RRTYPE_MG             8     /* a mail group member (EXPERIMENTAL) */
#define DNS_RRTYPE_MR             9     /* a mail rename domain name (EXPERIMENTAL) */
#define DNS_RRTYPE_NULL           10    /* a null RR (EXPERIMENTAL) */
#define DNS_RRTYPE_WKS            11    /* a well known service description */
#define DNS_RRTYPE_PTR            12    /* a domain name pointer */
#define DNS_RRTYPE_HINFO          13    /* host information */
#define DNS_RRTYPE_MINFO          14    /* mailbox or mail list information */
#define DNS_RRTYPE_MX             15    /* mail exchange */
#define DNS_RRTYPE_TXT            16    /* text strings */

/** DNS field CLASS used for "Resource Records" */
#define DNS_RRCLASS_IN            1     /* the Internet */
#define DNS_RRCLASS_CS            2     /* the CSNET class (Obsolete - used only for examples in some obsolete RFCs) */
#define DNS_RRCLASS_CH            3     /* the CHAOS class */
#define DNS_RRCLASS_HS            4     /* Hesiod [Dyer 87] */
#define DNS_RRCLASS_FLUSH         0x800 /* Flush bit */

typedef unsigned char	u8_t; 
typedef signed char		s8_t; 
typedef unsigned short	u16_t; 
typedef signed short	s16_t; 
typedef unsigned int	u32_t; 
typedef signed int		s32_t; 
//typedef unsigned int	mem_ptr_t; 

struct ip_info {
    struct ip_addr ip;
    struct ip_addr netmask;
    struct ip_addr gw;
};

enum {
	STATION_IF = 0,
	SOFTAP_IF,
	MAX_IF
};

// Starts the mDNS responder task, call first
void homekit_mdns_init(void);

// Build and advertise an appropriate linked set of PTR/TXT/SRV/A records for the parameters provided
// This is a simple canned way to build a set of records for a single service that will
// be advertised whenever the device is given an IP address by WiFi

typedef enum {
    mdns_TCP,
    mdns_UDP,
    mdns_Browsable        // see RFC6763:11 - adds a standard record that lets browsers find the service without needing to know its name
} mdns_flags;
    
void mdns_add_facility( const char* instanceName,   // Short user-friendly instance name, should NOT include serial number/MAC/etc
                        const char* serviceName,    // Must be registered, _name, (see RFC6335 5.1 & 5.2)
                        const char* addText,        // Should be <key>=<value>, or "" if unused (see RFC6763 6.3)
                        mdns_flags  flags,          // TCP or UDP plus browsable
                        u16_t       onPort,         // port number
                        u32_t       ttl             // time-to-live, seconds
                      );


// Low-level RR builders for rolling your own
void mdns_add_PTR(const char* rKey, u32_t ttl, const char* nameStr);
void mdns_add_SRV(const char* rKey, u32_t ttl, u16_t rPort, const char* targname);
void mdns_add_TXT(const char* rKey, u32_t ttl, const char* txtStr);
void mdns_add_A  (const char* rKey, u32_t ttl, ip_addr_t addr);

/* Sample usage, advertising a secure web service

    mdns_init();
    mdns_add_facility("Fluffy", "_https", "Zoom=1", mdns_TCP+mdns_Browsable, 443, 600);
   
*/

void mdns_thread(void);

#endif
