/*=============================================================================+
|                                                                              |
| Copyright 2012                                                               |
| Montage Inc. All right reserved.                                             |
|                                                                              |
+=============================================================================*/
/*-----------------------------------------------------------------------------+
| Common Macros                                                                |
+-----------------------------------------------------------------------------*/
#define CHA_DUTY		0xFFFFFE0F
#define CHB_DUTY		0xFFFFC1FF
#define CHA_TIME		0xFFFE3FFF
#define CHB_TIME		0xFFF1FFFF
#define CHA_POLAR		0xFFEFFFFF
#define CHB_POLAR		0xFFDFFFFF
#define PRE_SCALER		0xC03FFFFF

#define PWM1_A			0xFF8FFFFF
#define PWM1_B			0xFC7FFFFF
#define PWM2_A			0xE3FFFFFF
#define PWM2_B			0x1FFFFFFF

enum
{
	PWM_CHA_EN = 1,
	PWM_CHB_EN,
	PWM_EN,
	PWM_FREQ_ID_NUM = 31,
};

/*-----------------------------------------------------------------------------+
| Function Prototypes                                                          |
+-----------------------------------------------------------------------------*/
/** @defgroup pwm-api PWM Functions
 *  @{
 */
/**
 * @brief Set PWM frequency
 * @details PWM0 and PWM1 share the same frequency setting. PWM1's frequency will be changed, if user changes PWM0's frequency. (PWM2 and PWM3 as well)
 *
 * id:
 *
 * 0 : 0.1   Hz, 1 : 0.2   Hz, 2 : 0.5   Hz, 3 : 12    Hz, 4 : 20    Hz, 5 : 25    Hz,
 *
 * 6 : 50.34 Hz, 7 : 99.65 Hz, 8 : 180.84Hz, 9 : 305.17Hz, 10: 407   Hz, 11: 651   Hz,
 *
 * 12: 701   Hz, 13: 801   Hz, 14: 1    kHz, 15: 2    kHz, 16: 3    kHz, 17: 4    kHz,
 *
 * 18: 5.04 kHz, 19: 6.01 kHz, 20: 7.1  kHz, 21: 9.19 kHz, 22: 10.41kHz, 23: 12.02kHz,
 *
 * 24: 13.02kHz, 25: 19.53kHz, 26: 26.04kHz, 27: 39.06kHz, 28: 52.08kHz, 29: 78.13kHz,
 *
 * 30: 156.25kHz
 * @param pwm_ch 0 ~ 3 (GPIO 6 ~ 9)
 * @param id 0 ~ 30
 * @return None
 */
void pwm_set_freq(int pwm_ch, int id);
/**
 * @brief Set PWM duty
 * @param pwm_ch 0 ~ 3 (GPIO 6 ~ 9)
 * @param duty 0 ~ 255; 128 = 50%, 255 = 100%
 * @return None
 */
void pwm_set_duty(int pwm_ch, int duty);
/**
 * @brief Set PWM polarity
 * @param pwm_ch 0 ~ 3 (GPIO 6 ~ 9)
 * @param value 0: active low\n 1: active high
 * @return None
 */
void pwm_set_polarity(int pwm_ch, int value);
/**
 * @brief Set PWM enable
 * @details
 * UART channel 1 use the same pin as PWM0,1 (GPIO6,7)
 *
 * UART channel 2 use the same pin as PWM2,3 (GPIO8,9)
 * @param pwm_ch 0 ~ 3 (GPIO 6 ~ 9)
 * @param value 0: disable\n 1: enable
 * @return None
*/
void pwm_set_enable(int pwm_ch, int value);
/**
 * @brief Get PWM frequency
 * @param pwm_ch 0 ~ 3 (GPIO 6 ~ 9)
 * @return frequency id, -1: can't find correct id, -2: wrong parameter
 */
int pwm_get_freq(int pwm_ch);
/**
 * @brief Get PWM duty
 * @param pwm_ch 0 ~ 3(GPIO 6 ~ 9)
 * @return duty 0 ~ 255, -1: wrong parameter
 */
int pwm_get_duty(int pwm_ch);
/**
 * @brief Get PWM polarity
 * @param pwm_ch 0 ~ 3 (GPIO 6 ~ 9)
 * @return -1: wrong parameter 0: active low\n 1: active high
 */
int pwm_get_polarity(int pwm_ch);
/*! @} */
