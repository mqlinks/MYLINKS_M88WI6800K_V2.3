/*=============================================================================+
|                                                                              |
| Copyright 2012                                                               |
| Montage Inc. All right reserved.                                             |
|                                                                              |
+=============================================================================*/
/*! 
*   \file flash_api.h
*   \brief Flash API
*   \author Montage
*/

#ifndef _FLASH_API_H
#define _FLASH_API_H

enum
{
    FL_OK           = 0,
    FL_ERR          = -1,
    FL_ERR_PROG     = -2,
    FL_ERR_ERASE    = -3,
    FL_ERR_TIMEOUT  = -4,
    FL_ERR_UNSUPPORT= -5,
    FL_ERR_NOT_INIT = -6,
    FL_BOTTOM       = 1,
    FL_TOP          = 2,
    FL_PROG_UNIT = (1<<16),
};

typedef struct flashsec
{
    unsigned char size; // in power of 2
    unsigned short num; // number of sector
    unsigned char ecmd;
} flashsec;

struct flash_dev_tab
{
    unsigned long id;
    char flags;
    short sec;
    flashsec * sdesc;
    unsigned long size;
};
#define FLASH_APP_STARTADDR  0x20000
#define FLASH_OTA_STARTADDR  0x70000
#define FLASH_OTA_LENGTH     0x50000
#define USER_PARAM_FLASH_PART       0xC0000
#define USER_PARAM_FLASH_LENGTH       0x20000 // 128k bytes


/** @defgroup flash-api Flash Functions
 *  @{
 */
/**
 *  @brief erase flash with length
 *  @param addr: address (offset) on flash
 *  @param len: how many bytes to erase
 *  @return check result code
 */
int flash_erase (unsigned int addr, unsigned int len);
/**
 *  @brief Write to serial flash from buffer
 *  @param addr: address (offset) on flash
 *  @param len: how many bytes to write
 *  @param from: from buffer
 *  @return check result code
 */
int flash_write (unsigned int addr, unsigned int from, int len);
/**
 *  @brief read data from serial flash to buffer
 *  @param addr: address (offset) on flash
 *  @param to: from buffer
 *  @param numOfByte: how many bytes to write
 *  @return check result code
 */
unsigned int flash_read(unsigned int addr, unsigned int to, unsigned int numOfByte);
/*! @} */

#endif
