/*=============================================================================+
|                                                                              |
| Copyright 2012                                                               |
| Montage Inc. All right reserved.                                             |
|                                                                              |
+=============================================================================*/
/*!
*   \file   wla_api.h
*   \brief  define wla API functions
*   \author Montage
*/

#ifndef _WLA_API_H_
#define _WLA_API_H_

enum {
	NETIF_STA = 0,
	NETIF_AP,
};
/**
 *  @brief  wlan network interface enumeration definition.
 */
typedef enum {
	SOFT_AP,/**< Act as an access point, and other station can connect, 4 stations Max*/
	STATION	/**< Act as a station which can connect to an access point*/
} wlan_if_types;

/**
 *  @brief  Wi-Fi security type enumeration definition.
 */
typedef  enum {
	SECURITY_NONE,        /**< Open system. */
	SECURITY_WEP,         /**< Wired Equivalent Privacy. WEP security. */
	SECURITY_WPA_TKIP,    /**< WPA /w TKIP */
	SECURITY_WPA_AES,     /**< WPA /w AES */
	SECURITY_WPA2_TKIP,   /**< WPA2 /w TKIP */
	SECURITY_WPA2_AES,    /**< WPA2 /w AES */
	SECURITY_WPA2_MIXED,  /**< WPA2 /w AES or TKIP */
	SECURITY_AUTO,        /**< It is used when calling @ref wlan_start_adv, Wlan read security type from scan result. */
} security_types;

typedef enum {
	FILTER_RX_BEACON,     // RX wifi beacon
	FILTER_RX_PROBE_REQ,  // RX wifi probe request
	FILTER_RX_PROBE_RES,  // RX wifi probe response
	FILTER_RX_ACTION,     // RX wifi action packet
	FILTER_RX_MANAGEMENT, // RX all wifi management packet
	FILTER_RX_DATA,       // RX all wifi data packet
	FILTER_RX_MCAST_DATA, // RX all wifi the data packet which destination is broacast("FFFFFFFFFFFF") or IPv4 multicast MAC("01005Exxxxxx")
	FILTER_RX_CTRL,       // RX all wifi control packet
	FILTER_RX_MAX,
} filter_rx_types;

typedef enum {
	NOTIFY_WIFI_SCAN_COMPLETED,			//void (*function)(scan_result *pApList);
	NOTIFY_WIFI_STATUS_CHANGED,			//void (*function)(wifi_event status);
	NOTIFY_WIFI_PARA_CHANGED,			//void (*function)(apinfo_adv *ap_info, char *key, int key_len);
	NOTIFY_DHCP_COMPLETED,				//void (*function)(ip_sts *pnet);
	NOTIFY_DNS_RESOLVE_COMPLETED,		//void (*function)(char *str, int len);
	NOTIFY_WIFI_CONNECT_FAILED,			//void (*function)(int err);
	NOTIFY_WIFI_LED_TIMEOUT,			//int (*function)(void);
	/* User defined notifications */
} notify_types;

typedef enum {
	STATE_IDLE = 0,
	STATE_SCAN,
	STATE_SCAN_DONE,
	STATE_LINK_UP,
	STATE_LINK_DOWN,
} wlan_if_sm;

typedef enum {
	OPMODE_STA = 0,
	OPMODE_AP,
	OPMODE_APSTA,
} wlan_op_types;

typedef struct {
	char dhcp;		/**< DHCP mode: @ref DHCP_DISABLE, @ref DHCP_CLIENT, @ref DHCP_SERVER.*/
	char ip[16];	/**< Local IP address on the target wlan interface: @ref wlan_if_types.*/
	char gate[16];	/**< Router IP address on the target wlan interface: @ref wlan_if_types.*/
	char mask[16];	/**< Netmask on the target wlan interface: @ref wlan_if_types.*/
	char dns[16];	/**< DNS server IP address.*/
	char mac[16];	/**< MAC address, example: "C89346112233".*/
	char broadcastip[16];
} ip_sts;

struct _aplist_adv {
	char ssid[32];				/**< The SSID of an access point.*/
	char power;					/**< Signal strength, min:0, max:100*/
	unsigned char bssid[6];				/**< The BSSID of an access point.*/
	char channel;				/**< The RF frequency, 1-13*/
	security_types security;	/**< Security type, @ref security_types */
};

/***
 *  @brief  Scan result.
 */
typedef struct
{
	char ap_num;	/**< The number of access points found in scanning.*/
	void *aplist;
} scan_result;

/***
 *  @brief  Input network paras, used in wlan_start function.
 */
typedef struct
{
	char wifi_mode;				/**< Wi-Fi mode: @ref wlan_if_types.*/
	char wifi_ssid[32];			/**< SSID of the wlan needs to be connected.*/
	char wifi_key[64];			/**< Security key of the wlan needs to be connected, ignored in an open system.*/
	char local_ip_addr[16];		/**< Static IP configuration, Local IP address. */
	char net_mask[16];			/**< Static IP configuration, Netmask. */
	char gateway_ip_addr[16];	/**< Static IP configuration, Router IP address. */
	char dnssvr_ip_addr[16];	/**< Static IP configuration, DNS server IP address. */
	char dhcp_mode;				/**< DHCP mode, @ref DHCP_DISABLE, @ref DHCP_CLIENT and @ref DHCP_SERVER. */
	char reserved[32];
	int  wifi_retry_interval;	/**< Retry interval if an error is occured when connecting an access point, time unit is millisecond. */
} network_info;

/***
 *  @brief  Advanced precise wlan parameters, used in @ref network_info_adv.
 */
typedef struct
{
	char ssid[32];	/**< SSID of the wlan that needs to be connected. Example: "SSID String". */
	unsigned char bssid[6];	/**< BSSID of the wlan needs to be connected. Example: {0xC8 0x93 0x46 0x11 0x22 0x33}. */
	char channel;	/**< Wlan's RF frequency, channel 1-13. 1-13 means a fixed channel
							that can speed up a connection procedure, 0 is not a fixed input means all channels are possiable*/
	security_types security;
} apinfo_adv;

/***
 *  @brief  Input network precise paras in wlan_start_adv function.
 */
typedef struct
{
	apinfo_adv ap_info;			/**< @ref apinfo_adv. */
	char key[64];				/**< Security key or PMK of the wlan. */
	int  key_len;				/**< The length of the key. */
	char local_ip_addr[16];		/**< Static IP configuration, Local IP address. */
	char net_mask[16];			/**< Static IP configuration, Netmask. */
	char gateway_ip_addr[16];	/**< Static IP configuration, Router IP address. */
	char dnssvr_ip_addr[16];	/**< Static IP configuration, DNS server IP address. */
	char dhcp_mode;				/**< DHCP mode, @ref DHCP_DISABLE, @ref DHCP_CLIENT and @ref DHCP_SERVER. */
	char reserved[32];
	int  wifi_retry_interval;	/**< Retry interval if an error is occured when connecting an access point, time unit is millisecond. */
} network_info_adv;

/***
 *  @brief  Current link status in station mode.
 */
typedef struct {
	int is_connected;	/**< The link to wlan is established or not, 0: disconnected, 1: connected. */
	int wifi_strength;	/**< Signal strength of the current connected AP */
	char ssid[32];		/**< SSID of the current connected wlan */
	unsigned char bssid[6];		/**< BSSID of the current connected wlan */
	int  channel;		/**< Channel of the current connected wlan */
} link_sts;

typedef struct {
	unsigned char bssid[6];
	char ssid[33];
	char key[65];
	int  user_data_len;
	char user_data[65];
} omniconfig_result;

typedef struct {
	unsigned char mode;			/**< 0=disable, 1=always, 2=by RSSI */
	unsigned char max_cnt;		/**< resend maximum counter(0-15) */
	unsigned char min_rssi;		/**< resend minimum RSSI(0-75) */
	unsigned char reserved;
} resend_cfg;

#define NO_ERR							0		//! No error occurred.

// ==== extention for  wlan ====
#define WLAN_NO_ERR						0		/**< No error occurred in wlan operation. */
#define WLAN_ERR_PENDING				1		/**< Pending. */
#define WLAN_ERR_TIMEOUT				2		/**< Timeout occurred in wlan operation. */
#define WLAN_ERR_RESULT					3		/**< Partial results */
#define WLAN_ERR_KEY					4		/**< Invalid key */
#define WLAN_ERR_NOTEXIST				5		/**< Does not exist */
#define WLAN_ERR_AUTH					6		/**< Not authenticated */
#define WLAN_ERR_NOTKEY					7		/**< Not keyed */
#define WLAN_ERR_IOCTL					8		/**< IOCTL fail */
#define WLAN_NOT_BUFFER_READY_TEMP		9		/**< Buffer unavailable temporarily */
#define WLAN_NOT_BUFFER_READY_PERM		10		/**< Buffer unavailable permanently */
#define WLAN_WPS_PBC_OVERLAP			11		/**< WPS PBC overlap */
#define WLAN_CONN_LOST					12		/**< Connection lost */


#define WLAN_ERR_GENERAL				-1		/**< General error in wlan operation. */
#define WLAN_ERR_ARG					-2		/**< Wlan parameter is incorrect, missing, or not appropriate. */
#define WLAN_ERR_OPTION					-3		/**< Bad option */
#define WLAN_NOT_UP						-4		/**< Not up */
#define WLAN_NOT_DOWN					-5		/**< Not down */
#define WLAN_NOT_AP						-6		/**< Not AP */
#define WLAN_NOT_STA					-7		/**< Not STA  */
#define WLAN_ERR_KEYIDX					-8		/**< BAD Key Index */
#define WLAN_RADIO_OFF					-9		/**< Radio Off */
#define WLAN_NOT_BANDLOCKED				-10		/**< Not  band locked */
#define WLAN_ERR_CLK					-11		/**< No Clock */
#define WLAN_ERR_RATE					-12		/**< BAD Rate valueset */
#define WLAN_ERR_BAND					-13		/**< BAD Band */
#define WLAN_BUFFER_SHORT				-14		/**< Buffer too short */
#define WLAN_BUFFER_LONG				-15		/**< Buffer too long */
#define WLAN_BUSY						-16		/**< Busy */
#define WLAN_NOT_ASSOCIATED				-17		/**< Not Associated */
#define WLAN_ERR_SSIDLEN				-18		/**< Bad SSID len */
#define WLAN_ERR_CHANNELRANGE			-19		/**< Out of Range Channel */
#define WLAN_ERR_CHANNEL				-20		/**< Bad Channel */
#define WLAN_ERR_ADDR					-21		/**< Bad Address */
#define WLAN_ERR_RESOURCES				-22		/**< Not Enough Resources */
#define WLAN_UNSUPPORTED				-23		/**< Unsupported */
#define WLAN_ERR_LENGTH					-24		/**< Bad length */
#define WLAN_ERR_NOTREADY				-25		/**< Not Ready */
#define WLAN_ERR_NOTPERMIT				-26		/**< Not Permitted */
#define WLAN_ERR_MEMORY					-27		/**< No Memory */
#define WLAN_ASSOCIATED					-28		/**< Associated */
#define WLAN_ERR_RANGE					-29		/**< Not In Range */
#define WLAN_ERR_NOTFOUND				-30		/**< Not Found */
#define WLAN_NOT_WME					-31		/**< WME Not Enabled */
#define WLAN_NOT_TSPEC					-32		/**< TSPEC Not Found */
#define WLAN_NOT_ACM					-33		/**< ACM Not Supported */
#define WLAN_NOT_WMEASSOCIATED			-34		/**< Not WME Association */
#define WLAN_ERR_SDIOBUS				-35		/**< SDIO Bus Error */
#define WLAN_NOT_ACCESSIBLE				-36		/**< WLAN Not Accessible */
#define WLAN_ERR_VERSION				-37		/**< Incorrect version */
#define WLAN_ERR_TX						-38		/**< TX failure */
#define WLAN_ERR_RX						-39		/**< RX failure */
#define WLAN_ERR_NODEVICE				-40		/**< Device not present */
#define WLAN_ERR_UNFINISHED				-41		/**< To be finished */
#define WLAN_DISABLED					-43		/**< Disabled in this build */
#define WLAN_ERR_LAST					-44

#define DHCP_DISABLE	(0)	/**< Disable DHCP service. */
#define DHCP_CLIENT		(1)	/**< Enable DHCP client which get IP address from DHCP server
								automatically, reset Wi-Fi connection if failed. */
#define DHCP_SERVER		(2)	/**< Enable DHCP server, needs assign a static address as local address. */
#define DHCP_APSTA_SERVER (3)
typedef enum {
	NOTIFY_STA_LINK_UP = 1,
	NOTIFY_STA_LINK_DOWN,

	NOTIFY_AP_LINK_UP,
	NOTIFY_AP_LINK_DOWN,
} wifi_event;

typedef void (*monitor_cb_t)(void *desc, char rssi);

/** @defgroup wlan-api WLAN Functions
 *  @{
 */

/**
 * @brief Initialize Wi-Fi basic settings.
 * @details The function initialize Wi-Fi basic settings.
 * @return None.
 */
void wlan_init(void);
/** @brief  Connect or establish a Wi-Fi network in normal mode (station or soft ap mode).
 *  @details This function can establish a Wi-Fi connection as a station or create
 *          a soft AP that other stations can connect (4 stations Max). In station mode,
 *          Wlan first scan all of the supported Wi-Fi channels to find a wlan that
 *          matches the input SSID, and read the security mode. Then try to connect
 *          to the target wlan. If any error occurs in the connection procedure or
 *          disconnected after a successful connection, Wlan start the reconnection
 *          procedure in background after a time interval defined in inNetworkInitPara.
 *          Call this function twice when setup coexistence mode (station + soft ap).
 *          This function returns immediately in station mode, and the connection will
 *          be executed in background.
 *  @param  net: Specifies wlan parameters.
 *  @return In station mode, always return WLAN_NO_ERR.
 *          In soft ap mode, return WLANXXXERR
 */
int wlan_start(network_info *net);
/** @brief  Connect to a Wi-Fi network with advantage settings (station mode only)
 *  @details This function can connect to an access point with precise settings,
 *          that greatly speed up the connection if the input settings are correct
 *          and fixed. If this fast connection is failed for some reason, Wlan
 *          change back to normal: scan + connect mode refer to @ref wlan_start.
 *          This function returns after the fast connection try.
 *  @note   This function cannot establish a soft ap, use wlan_start() for this
 *          purpose.
 *          If input SSID length is 0, Wlan use BSSID to connect the target wlan.
 *          If both SSID and BSSID are all wrong, the connection will be failed.
 *  @param  net: Specifies the precise wlan parameters.
 *  @return Always return WLAN_NO_ERR although error occurs in first fast try.
 *          Return WLAN_ERR_TIMEOUT if DHCP client timeout.
 */
int wlan_start_adv(network_info_adv *net);
/** @brief  Read current wireless link status.
 *  @param  sts: Point to the buffer to store the link status.
 *  @param  type Specifies wlan interface.
 *  @retval NO_ERR.
 */
int wlan_get_link_sts(link_sts *sts, wlan_if_types type);
/** @brief  Start a wlan scanning in 2.4GHz in background.
 *  @details Once the scan is completed, Wlan sends a notify:
 *          NOTIFY_WIFI_SCAN_COMPLETED, with callback function:
 *          void (*function)(scan_result *pApList, Context_t * const inContext).
 *          Register callback function using @ref wlan_add_notification() before scan.
 */
void wlan_start_scan(void);
/** @brief  Close the RF chip's power supply, all network connection is lost.
 *  @param  cb: Power off callback function.
 *  @return NO_ERR: succeed\n WLAN_ERR_GENERAL: failed
 */
int wlan_power_off(void (*cb)(void));
/** @brief  Open the RF's power supply and do some necessary initialization.
 *  @note   The defaut RF state is powered on after @ref wlan_init, so this function is
 *          not needed after wlan_init.
 *  @return NO_ERR: succeed\n WLAN_ERR_GENERAL: failed
 */
int wlan_power_on(void);
/**@brief  Close all the Wi-Fi connections, station mode and soft ap mode.
 * @note   This function also stop the background retry mechanism started by
 *         wlan_start() and wlan_start_adv().
 * @return NO_ERR.
 */
int wlan_suspend(void);
/** @brief  Close the connection in station mode.
 *  @note   This function also stop the background retry mechanism started by
 *          wlan_start() and wlan_start_adv().
 *  @return NO_ERR.
 */
int wlan_suspend_station(void);
/** @brief  Enable IEEE power save mode
 *  @details When this function is enabled, Wlan enter IEEE power save mode if
 *          Wlan is in station mode and has connected to an AP, and do not need
 *          any other control from application.
 *  @param mode 2: PSnonPOLL\n 1: PSPOLL
 *  @retval NO_ERR succeed
 *  @retval WLAN_ERR_GENERAL failed
 */
int wlan_enable_powersave(int mode);
/**
 * @brief  Disable IEEE power save mode
 * @retval NO_ERR succeed
 * @retval WLAN_ERR_GENERAL failed
 */
int wlan_disable_powersave(void);
/**
 * Set which wifi packet will be captured.
 * RX all wifi packet if didn't call this function.
 * This function can be called more than once to set RX different type packet.
 * @param type Capture packet type.
 * @return NO_ERR.
 */
int wlan_monitor_rx_type(filter_rx_types type);
/**
 * Start wifi monitor.
 * @return NO_ERR.
 */
int wlan_start_monitor(void);
/**
 * Stop wifi monitor.
 * @return NO_ERR.
 */
int wlan_stop_monitor(void);
/**
 * @brief Set TX power level.
 * @details
 * The function sets TX power level. It checks range from 0 to 12
 * and restarts Wi-Fi if it is running.
 * @param level The TX power level.
 * @return 0: succeed\n -1: failed
 */
int wlan_set_txpwr(int level);
/**
 * @brief Set Wi-Fi physical mode.
 * @details The function sets Wi-Fi physical mode. It checks range from 0 to 7
 * (bit[2:0]=ngb) and restarts Wi-Fi if it is running.
 * @param phy The physical mode.
 * @return 0: succeed\n -1: failed\n -2: busy
 */
int wlan_set_phy(int phy);
/**
 * @brief Set the monitor channel.
 * @details Set the monitor channel. Valid channel is 1~13.
 * In soft-AP + station mode, soft-AP will adjust its channel configuration
 * to be as same as station and the API will return NO_ERR.
 * @param channel Monitor channel.
 * @return NO_ERR.
 */
int wlan_set_channel(int channel);
/**
 * @brief Set the monitor channel.
 * @details Set the monitor channel and bandwith. Valid channel is 1~13.
 * Set ht40 as 0 for 20Mhz, ht40 as 1 for 40Mhz.
 * In soft-AP + station mode, soft-AP will adjust its channel configuration
 * to be as same as station and the API will return NO_ERR.
 * @param channel Monitor channel.
 * @param ht40 Bandwidth is 40M-Hz.
 * @return NO_ERR.
 */
int wlan_set_ch_bandwidth(int channel,int ht40);
/**
 * Set the callback function to RX the captured wifi packet.
 * @param fn Callback function.
 * @return None.
 */
void wlan_register_monitor_cb(monitor_cb_t fn);
/**
 * The function initializes notification center.
 * @return NO_ERR.
 */
int wlan_init_notification(void);
/**
 * The function register notification and it's callback function.
 * @param type system defined notifications.
 * @param functionAddress callback function.
 * @return NO_ERR.
 */
int wlan_add_notification(notify_types type, void *functionAddress);
/**
 * The function unregister notification and it's callback function.
 * @param type system defined notifications.
 * @return NO_ERR.
 */
int wlan_del_notification(notify_types type);
/**
 * The function unregister all notification and callback functions.
 * @param type system defined notifications.
 * @return NO_ERR.
 */
int wlan_del_notification_all(notify_types type);
/**
 * Set Wi-Fi network MAC address.
 * @param  type Specifies wlan interface.
 * @param  myaddr Interface MAC address.
 * @return None.
 */
void wlan_set_myaddr(wlan_if_types type, char *myaddr);
/**
 * Get Wi-Fi network MAC address.
 * @param  type Specifies wlan interface.
 * @return Interface MAC address.
 */
char *wlan_get_myaddr(wlan_if_types type);
/**
 * Set Wi-Fi network hidden ssid status (ap mode only).
 * @param  en Hidden ssid or not.
 * @return None.
 */
void wlan_set_hidden_ssid(int en);
/**
 * Get Wi-Fi network hidden ssid status (ap mode only).
 * @return hidden ssid status.
 */
int wlan_get_hidden_ssid(void);
/**
 * @brief Set reconnect policy in STA mode.
 * @details The function sets reconnect policy in STA mode. If the policy
 * is enabled, the STA will reconnect to AP once it's disconnected.
 * @param en 1: enable\n 0: disable
 * @return None.
 */
void wlan_set_reconnect(int en);
/**
 * @brief Get reconnect policy in STA mode.
 * @details The function gets reconnect policy in STA mode.
 * @return 1: enabled\n 0: disabled
 */
int wlan_get_reconnect(void);
/**
 * @brief Set resend mode, maximum resend counter, and minimum resend RSSI.
 * @details The function sets resend mode, maximum resend counter,
 * and minimum resend RSSI.
 * @param resend_mode 0: disable\n 1: always\n 2:by RSSI
 * @param resend_max_cnt 0-15
 * @param resend_min_rssi 0-75
 * @return None.
 */
void wlan_set_resend_params(unsigned int resend_mode,
							unsigned int resend_max_cnt,
							unsigned int resend_min_rssi);
/**
 * @brief Get resend parameters.
 * @details The function gets resend parameters.
 * @param  cfg: Point to the buffer to store the resend cfg.
 * @return NO_ERR: succeed
 */
int wlan_get_resend_params(resend_cfg *cfg);
/**
 * @brief Set HT 40MHz mode.
 * @details The function sets HT 40MHz mode.
 * @param en 1: enable\n 0: disable
 * @return None.
 */
void wlan_set_ht40(int en);
/**
 * @brief Get HT 40MHz mode status.
 * @details The function gets HT 40MHz mode status.
 * @return 1: enabled\n 0: disabled
 */
int wlan_get_ht40(void);
/**
 * @brief Set scan channel, scan channel 1-13 if argument is zero.
 * @details The function sets scan channel, scan channel 1-13 if argument is zero.
 * @param channel 1-13: scan specific channel\n 0: scan channel 1-13
 * @return NO_ERR: succeed\n WLAN_ERR_GENERAL: failed
 */
int wlan_set_scan_channel(int channel);
/**
 * @brief Get scan channel.
 * @details The function gets scan channel.
 * @return 0-13
 */
int wlan_get_scan_channel(void);
/**
 * @brief Get Wi-Fi network connection number.
 * @details The function gets Wi-Fi station number.
 * @param type: Specifies wlan interface.
 * @return -1: failed\n otherwise: station number
 */
int wlan_get_sta_num(wlan_if_types type);
/**
 * @brief Gets Wi-Fi station information (ap mode only).
 * @details The function gets Wi-Fi station information (ap mode only).
 * @param get_mac_callback The callback function.
 * @return 0: succeed\n -1: failed
 */
int wlan_get_sta_info(int (*get_mac_callback)(const char *));
/**
 * @brief Gets Wi-Fi connection status.
 * @details The function gets Wi-Fi connection status.
 * @param type: Specifies wlan interface.
 * @return -1: failed
 * -        0: STATE_IDLE
 * -        1: STATE_SCAN
 * -        2: STATE_SCAN_DONE
 * -        3: STATE_LINK_UP
 * -        4: STATE_LINK_DOWN
 */
int wlan_get_ifs_sm(wlan_if_types type);
/**
 * @brief Dump all APs information into apps buffer.
 * @details The function dumps information of all APs into apps buffer
 * without output to serial port.
 * @return None.
 */
void wlan_scan_result_to_buffer(void);
/**
 * The function starts to service Wi-Fi LED callback function.
 * @return None.
 */
void wlan_led_install(void);
/**
 * The function stop to service Wi-Fi LED callback function.
 * @return None.
 */
void wlan_led_uninstall(void);
/*! @} */
/** @brief  Stop soft ap and close all stations' connections.
 *  @return NO_ERR.
 */
int wlan_suspend_softap(void);

#define WIFI_RST_PIN	2
#define WIFI_LED_PIN	15

#endif
