/*=============================================================================+
|                                                                              |
| Copyright 2015                                                             |
| Montage Inc. All right reserved.                                             |
|                                                                              |
+=============================================================================*/
/*! 
*   \file app_init.c
*   \brief main entry
*   \author Montage
*/

/*=============================================================================+
| Included Files                                                               |
+=============================================================================*/
#include <stdint.h>
#include <stdarg.h>
#include <common.h>
#include <flash_api.h>
#include "lwip/sockets.h"
#include <lynx_debug.h>
#include <gpio.h>
#include <serial.h>
#include <event.h>
#include <net_api.h>
#include <wbuf.h>
#include <wla_api.h>
#include <gpio.h>
#include <cfg_api.h>
#include <user_config.h>
#include <FreeRTOS.h>
#include <task.h>
#include "semphr.h"
#include <net_api.h>
#include <socket_api.h>
#include <mylinks_wifi.h>


#define MONITOR_DEBUG 1
#define ETH_ALEN (6)
#define CHANNEL_MAC_SAVE_LEN 256

extern uint32_t urbase[];
extern unsigned int  uart_rx_delay;
extern struct serial_buffer* ur1_rxbuf;
extern char *strchr(char* _Str,char _Ch);
static uint8_t at_cmdLine[at_cmdLenMax];
static uint16_t g_delay = 200;
static uint16_t g_channel = 0xffff;
static uint8_t trans_txb[1024];

static const uint8_t debug_mac[6]={0x80,0xad,0x16,0xe6,0x42,0x73};
//static const uint8_t debug_mac[6]={0x70,0x47,0xe9,0x79,0x75,0x7d};
rf_param g_rfParam;
sdk_param g_atParam =
{
	0,				//check_sum
	15, 			//led value
	3,				//dhcp mode
	{				//servo_moter_param
		1,
		0,
	},
	{				//dc_moter_param
		0,
		0,
	},
	{				//mac
	},
	{
		115200,		//baudrate;
		8,    		//databits;
		1,			//stopbits
		0,			//parity
		0,			//flowctrl
	},
	{
		"192.168.1.112",		//ip
		"255.255.255.0",		//mask
		"192.168.1.1",			//gw
		"114.114.114.114",
	},
	{
		"192.168.4.1",		//ip
		"255.255.255.0",	//mask
		"192.168.4.1",		//gw
	},
	{
		"192.168.4.100",
		"192.168.4.200",
		1440,
	},
	{
		true,                //trans mode when start
		//"www.mqlinks.com",
		"192.168.4.1",
		6000,
		NO_DNS|SERVER|TCP,
		//"TCP",
		300,
	},
	{
		false,                //trans mode when start
		"192.168.1.118",
		5808,
		NO_DNS|SERVER|UDP,
		//"TCP",
		0,
	},
	0,
	1,
	{
		"admin",
		"admin",		
	},
	SEC_CFG_MAGIC,		//HEAD
};
/*=============================================================================+
| Define                                                                       |
+=============================================================================*/
struct ieee80211_hdr {
    u16 frame_control;
    u16 duration_id;
    u8 addr1[ETH_ALEN];
    u8 addr2[ETH_ALEN];
    u8 addr3[ETH_ALEN];
    u16 seq_ctrl;
    u8 addr4[ETH_ALEN];
};


/*----------------------------------------------------------------*/
/**
 * The function is called once application start-up. Users can initial
 * structures or global parameters here.
 *
 * @param None.
 * @return int Protothread state.
 */
/*----------------------------------------------------------------*/
int app_main(void)
{
#ifdef CONFIG_WLA_LED
	pin_mode(WIFI_LED_PIN, 1);
	digital_write(WIFI_LED_PIN, 0);
#endif
	/* Do not add any process here, user should add process in user_thread */
	hw_sys_init();
	return PT_EXITED;
}

static char buf_t[512];

static uint8_t ap[6];
static uint8_t mac_t[6] = {0};
//static uint8_t mac_t[6] = {0xff,0xff,0xff,0xff,0xff,0xff};
static uint8_t temp_mac[CHANNEL_MAC_SAVE_LEN][6]={{0}};
static int temp_mac_len = 0;

static void temp_mac_init(void)
{
	temp_mac_len = 0;
}

static int8_t temp_mac_check(uint8_t *mac)
{
	int len = 0;
	for(len = 0;len < temp_mac_len;len++)
	{
		if(!memcmp(temp_mac[len],mac,6))
		{
			return 1;
		}
	}
	if(temp_mac_len >= CHANNEL_MAC_SAVE_LEN)
	{
		return 0;
	}
	memcpy(temp_mac[len],mac,6);
	temp_mac_len ++;
	return 0;
}

#define CPU_TO_U16_LE(x) \
   ((unsigned short)(\
      (((unsigned short)(x)& (unsigned short)0x00ffU)<<8) | \
      (((unsigned short)(x)& (unsigned short)0xff00U)>>8) ))




int LOG(const char *fmt,...)
{
	int len;
	va_list args;
    va_start(args, fmt);
    len = vsprintf(buf_t, fmt, args);
    va_end(args);
    uart0_tx_buffer(buf_t,len);
	return len;
}

static void monitor_cb(void *desc, char rssi)
{
	struct wbuf *wb = (struct wbuf *)desc;
	uint8_t *packet = (uint8_t *)wb + WB_DATAOFF(wb);
	int len = WB_PKTLEN(wb);
	struct ieee80211_hdr *hdr = (struct ieee80211_hdr *)packet;

		LOG("1:%02x:%02x:%02x:%02x:%02x:%02x\r\n",hdr->addr1[0],hdr->addr1[1],hdr->addr1[2],hdr->addr1[3],hdr->addr1[4],hdr->addr1[5]);
		LOG("2:%02x:%02x:%02x:%02x:%02x:%02x\r\n",hdr->addr2[0],hdr->addr2[1],hdr->addr2[2],hdr->addr2[3],hdr->addr2[4],hdr->addr2[5]);
		LOG("3:%02x:%02x:%02x:%02x:%02x:%02x\r\n",hdr->addr3[0],hdr->addr3[1],hdr->addr3[2],hdr->addr3[3],hdr->addr3[4],hdr->addr3[5]);
		LOG("FC:%x,len:%d,seq:%d\r\n\r\n",hdr->frame_control,len,hdr->seq_ctrl);
}

static void open_monitor(void)
{
    //printf("Start to recv airkiss's packets\n");
    wlan_monitor_rx_type(FILTER_RX_BEACON);
    wlan_monitor_rx_type(FILTER_RX_CTRL);
    wlan_monitor_rx_type(FILTER_RX_MANAGEMENT);
    wlan_monitor_rx_type(FILTER_RX_ACTION);
    wlan_monitor_rx_type(FILTER_RX_PROBE_REQ);
    wlan_monitor_rx_type(FILTER_RX_PROBE_RES);
    wlan_monitor_rx_type(FILTER_RX_MCAST_DATA);
    wlan_start_monitor();
    return;
}


void uart_init(void)
{
	int threshold = 0;	
	serial_conf(7, 0, 1, 0,threshold );
	serial_init((int)0);

#ifdef CONFIG_UR_FLOW_CONTROL
	pin_mode(CONFIG_UR_CTS, 0);
	digital_write(CONFIG_UR_RTS, 0);//RTS active
	pin_en_intr(CONFIG_UR_CTS, 0, (gpio_func_cb)uart_flow_cts_stat, NULL);//0: rising
	pin_en_intr(CONFIG_UR_CTS, 1, (gpio_func_cb)uart_flow_cts_stat, NULL);//1: falling
	pin_mode(CONFIG_UR_RTS, 1);
#endif

}



static uint8_t find_next_channel(uint8_t channel)
{

	uint8_t i;

	for(i = 0;i< 13;i++)
	{
		++channel;
		if(channel > 13) channel = 1;
		if(g_channel & 1 <<(channel - 1))
		{
			return channel;
		}
	}
	return 0;
}





int user_thread(void *arg )
{
	uint8_t channel = 1;
	uint8_t led_sts = 0;
	uart_init();
	wlan_register_monitor_cb(monitor_cb);
	open_monitor();
	for(;;)
	{
		digital_write(WIFI_LED_PIN, led_sts);
		led_sts = !led_sts;
		wlan_stop_monitor();
		temp_mac_init();
		channel = find_next_channel(channel);
		if(channel == 0)
		{
			goto channel_err;
		}
		wlan_set_channel(channel);
		open_monitor();
channel_err:
		sys_msleep(g_delay);
	}
exit:
	vTaskDelete(NULL);
}


