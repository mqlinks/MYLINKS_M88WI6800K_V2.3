/*=============================================================================+
|                                                                              |
| Copyright 2012                                                               |
| Montage Inc. All right reserved.                                             |
|                                                                              |
+=============================================================================*/
/**
 * @file cfg_api.c
 * @brief Config APIs
 *
 */
#include <stdint.h>
#include <event.h>
#include <lynx_dev.h>
#include <arch/ba22/chip.h>
#include <mt_types.h>
#include <common.h>
#include <lynx_debug.h>
#include <cfg_api.h>
#include <wla_api.h>
#include <built_info.h>
#include <version.h>
#include <user_config.h>
#include <serial.h>
#include <FreeRTOS.h>
#include <task.h>
#include "semphr.h"


const char *sw_build_time = (char *)SW_BUILD_TIME;
const char *sw_build_sdk = (char *)MT_SDK_VER_STR;
const int sw_build_count = SW_BUILD_COUNT;


/**
 * Config load data to memory, initialize config read/write data process.
 * Call config_load before read/write config data.
 * @return config memory default length 0x400, 0: error
 */




