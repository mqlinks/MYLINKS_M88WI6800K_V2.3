/*=============================================================================+
|                                                                              |
| Copyright 2015                                                               |
| Montage Inc. All right reserved.                                             |
|                                                                              |
+=============================================================================*/
/*!
*   \file   rlib/mem.c
*   \brief  Memory allocation API functions
*   \author Montage
*/
#include <arch/irq.h>
//#include <stddef.h>              // for size_t
#include <stdlib.h>
#include <string.h>
#include <cmd.h>
#include <common.h>
#include <lynx_dev.h>
#include <lynx_debug.h>

#define CONFIG_CMD_MI 1

#if 0
#define CONFIG_MM_REGIONS 1
/* #define CONFIG_SMALL_MEMORY */

#ifdef CONFIG_SMALL_MEMORY
# define MM_MIN_SHIFT      4  /* 16 bytes */
# define MM_MAX_SHIFT     15  /* 32 Kb */
#else
# define MM_MIN_SHIFT      4  /* 16 bytes */
# define MM_MAX_SHIFT     18  /* 256 Kb */
#endif

#ifdef CONFIG_SMALL_MEMORY
# define SIZEOF_MM_ALLOCNODE   4
#else
# define SIZEOF_MM_ALLOCNODE   8
#endif

#ifdef CONFIG_SMALL_MEMORY
# define MM_ALLOC_BIT    0x8000
#else
# define MM_ALLOC_BIT    0x80000000
#endif

#ifdef CONFIG_SMALL_MEMORY
# define SIZEOF_MM_FREENODE     8
#else
# define SIZEOF_MM_FREENODE    16
#endif

/* All other definitions derive from these two */

#define MM_MIN_CHUNK     (1 << MM_MIN_SHIFT)
#define MM_MAX_CHUNK     (1 << MM_MAX_SHIFT)
#define MM_NNODES        (MM_MAX_SHIFT - MM_MIN_SHIFT + 1)

#define MM_GRAN_MASK     (MM_MIN_CHUNK-1)
#define MM_ALIGN_UP(a)   (((a) + MM_GRAN_MASK) & ~MM_GRAN_MASK)
#define MM_ALIGN_DOWN(a) ((a) & ~MM_GRAN_MASK)

struct mm_allocnode_s
{
  size_t size;           /* Size of this chunk */
  size_t preceding;      /* Size of the preceding chunk */
};
struct mm_freenode_s
{
  size_t size;                     /* Size of this chunk */
  size_t preceding;                /* Size of the preceding chunk */
  struct mm_freenode_s *flink; /* Supports a doubly linked list */
  struct mm_freenode_s *blink;
};

size_t g_heapsize;
struct mm_allocnode_s *g_heapstart[CONFIG_MM_REGIONS];
struct mm_allocnode_s *g_heapend[CONFIG_MM_REGIONS];
struct mm_freenode_s g_nodelist[MM_NNODES];
#endif

#if CONFIG_MM_REGIONS > 1
int g_nregions;
#endif

#ifdef DEBUG_MEM_CORRUPT
void *memory_district = 0;
#define MEM_WARNING
#endif

/* Convert the size to a nodelist index */

static int mm_size2ndx(size_t size)
{
  int ndx = 0;

  if (size >= MM_MAX_CHUNK)
    {
       return MM_NNODES-1;
    }

  size >>= MM_MIN_SHIFT;
  while (size > 1)
    {
      ndx++;
      size >>= 1;
    }

  return ndx;
}

static void mm_addfreechunk(struct mm_freenode_s *node)
{
  struct mm_freenode_s *next;
  struct mm_freenode_s *prev;

  /* Convert the size to a nodelist index */

  int ndx = mm_size2ndx(node->size);

  /* Now put the new node int the next */

  for (prev = &ldev->mm.nodelist[ndx], next = ldev->mm.nodelist[ndx].flink;
       next && next->size && next->size < node->size;
       prev = next, next = next->flink);

  /* Does it go in mid next or at the end? */

  prev->flink = node;
  node->blink = prev;
  node->flink = next;

  if (next)
    {
      /* The new node goes between prev and next */

      next->blink = node;
    }
}

/*----------------------------------------------------------------*/
/**
 * Initialize the selected heap data structures, providing the initial
 * heap region.
 *
 * @param heapstart Start of the initial heap region.
 * @param heapsize Size of the initial heap region.
 * @return None.
 */
/*----------------------------------------------------------------*/
void mm_addregion(void *heapstart, size_t heapsize)
{
  struct mm_freenode_s *node;
  size_t heapbase;
  size_t heapend;
#if CONFIG_MM_REGIONS > 1
  int IDX = g_nregions;
#else
# define IDX 0
#endif

  /* Adjust the provide heap start and size so that they are
   * both aligned with the MM_MIN_CHUNK size.
   */

  heapbase = MM_ALIGN_UP((size_t)heapstart);
  heapend  = MM_ALIGN_DOWN((size_t)heapstart + (size_t)heapsize);
  heapsize = heapend - heapbase;

  //serial_printf("Region %d: base=%p size=%u\n", IDX+1, heapstart, heapsize);

  /* Add the size of this region to the total size of the heap */

  ldev->mm.heapsize += heapsize;

  /* Create two "allocated" guard nodes at the beginning and end of
   * the heap.  These only serve to keep us from allocating outside
   * of the heap.
   * 
   * And create one free node between the guard nodes that contains
   * all available memory.
   */

  ldev->mm.heapstart[IDX]            = (struct mm_allocnode_s *)heapbase;
  ldev->mm.heapstart[IDX]->size      = SIZEOF_MM_ALLOCNODE;
  ldev->mm.heapstart[IDX]->preceding = MM_ALLOC_BIT;

  node                        = (struct mm_freenode_s *)(heapbase + SIZEOF_MM_ALLOCNODE);
  node->size                  = heapsize - 2*SIZEOF_MM_ALLOCNODE;
  node->preceding             = SIZEOF_MM_ALLOCNODE;

  ldev->mm.heapend[IDX]              = (struct mm_allocnode_s *)(heapend - SIZEOF_MM_ALLOCNODE);
  ldev->mm.heapend[IDX]->size        = SIZEOF_MM_ALLOCNODE;
  ldev->mm.heapend[IDX]->preceding   = node->size | MM_ALLOC_BIT;

#undef IDX

#if CONFIG_MM_REGIONS > 1
  g_nregions++;
#endif

  /* Add the single, large free node to the nodelist */

  mm_addfreechunk(node);

  DBG_PRINTF(INFO, "Memory initial 0x%x free size:%d\n", heapstart, heapsize);
}

void initMem(void)
{
	int i;
	
	/* Set up global variables */

  ldev->mm.heapsize = 0;

#if CONFIG_MM_REGIONS > 1
  g_nregions = 0;
#endif

  /* Initialize the node array */

  memset(ldev->mm.nodelist, 0, sizeof(struct mm_freenode_s) * MM_NNODES);
  for (i = 1; i < MM_NNODES; i++)
    {
      ldev->mm.nodelist[i-1].flink = &ldev->mm.nodelist[i];
      ldev->mm.nodelist[i].blink   = &ldev->mm.nodelist[i-1];
    }

  /* Initialize the malloc semaphore to one (to support one-at-
   * a-time access to private data sets.
   */
	// May we have to use semaphore
  //mm_seminitialize();
    /* Add the initial region of memory to the heap */

#ifdef MEM_INFO_DEBUG       
  fun_add_mem_info = (int (*)(void *, int ))add_mem_info;
  fun_remove_mem_info = (int (*)(void *))remove_mem_info;
#endif 
        
}	

/****************************************************************************
 * Name: mm_initialize
 *
 * Description:
 *   Initialize the selected heap data structures, providing the initial
 *   heap region.
 *
 * Parameters:
 *   heapstart - Start of the initial heap region
 *   heapsize  - Size of the initial heap region
 *
 * Return Value:
 *   None
 *
 * Assumptions:
 *
 ****************************************************************************/
//#if (CONFIG_ROM_VER > 2)
#if 0
void mm_initialize(void *heapstart, size_t heapsize)
{
	initMem();/*reinit nodelist[].*/
	mm_addregion(heapstart, heapsize);
}
#endif

void  mm_shrinkchunk(struct mm_allocnode_s *node, size_t size)
{
  struct mm_freenode_s *next;

  /* Get a reference to the next node */

  next = (struct mm_freenode_s*)((char*)node + node->size);

  /* Check if it is free */

  if ((next->preceding & MM_ALLOC_BIT) == 0)
    {
      struct mm_allocnode_s *andbeyond;
      struct mm_freenode_s *newnode;

      /* Get the chunk next the next node (which could be the tail chunk) */

      andbeyond = (struct mm_allocnode_s*)((char*)next + next->size);

      /* Remove the next node.  There must be a predecessor, but there may
       * not be a successor node.
       */

      //DEBUGASSERT(next->blink);
      next->blink->flink = next->flink;
      if (next->flink)
        {
          next->flink->blink = next->blink;
        }

      /* Create a new chunk that will hold both the next chunk
       * and the tailing memory from the aligned chunk.
       */

      newnode = (struct mm_freenode_s*)((char*)node + size);

      /* Set up the size of the new node */

      newnode->size        = next->size + node->size - size;
      newnode->preceding   = size;
      node->size           = size;
      andbeyond->preceding = newnode->size | (andbeyond->preceding & MM_ALLOC_BIT);

      /* Add the new node to the freenodelist */

      mm_addfreechunk(newnode);
    }

  /* The next chunk is allocated.  Try to free the end portion
   * at the end chunk to be shrunk.
   */

  else if (node->size >= size + SIZEOF_MM_FREENODE)
    {
      struct mm_freenode_s *newnode;

      /* Create a new chunk that will hold both the next chunk
       * and the tailing memory from the aligned chunk.
       */

      newnode = (struct mm_freenode_s*)((char*)node + size);

      /* Set up the size of the new node */

      newnode->size        = node->size - size;
      newnode->preceding   = size;
      node->size           = size;
      next->preceding      = newnode->size | MM_ALLOC_BIT;

      /* Add the new node to the freenodelist */

      mm_addfreechunk(newnode);
    }
}

/*----------------------------------------------------------------*/
/**
 * The malloc function allocates space for an object whose size is
 * specified by size and whose value is indeterminate.
 *
 * @param size Size, in bytes, of the region to allocate.
 * @return NULL is returned if the space could not be allocated.
 * Otherwise, a pointer to a region of the requested size is returned.
 */
/*----------------------------------------------------------------*/
void *malloc( size_t size )
{
	struct mm_freenode_s *node;
  void *ret = NULL;
  int ndx;
  unsigned int save;
#ifdef MEM_WARNING
  int ret_addr = (int)__builtin_return_address(0);
#endif
  /* Handle bad sizes */
  if (size <= 0)
    {
#ifdef MEM_WARNING
		serial_printf("MEM WARNING: malloc zero size, ret_addr=%x\n",ret_addr);
#endif
      return NULL;
    }

  /* Adjust the size to account for (1) the size of the allocated
   * node and (2) to make sure that it is an even multiple of
   * our granule size.
   */

  size = MM_ALIGN_UP(size + SIZEOF_MM_ALLOCNODE);

  /* We need to hold the MM semaphore while we muck with the
   * nodelist.
   */
#if (CONFIG_ROM_VER > 1)
	save = irq_save();
#endif
  //mm_takesemaphore();

  /* Get the location in the node list to start the search.
   * Special case really big alloctions
   */

  if (size >= MM_MAX_CHUNK)
    {
      ndx = MM_NNODES-1;
    }
  else
    {
      /* Convert the request size into a nodelist index */

      ndx = mm_size2ndx(size);
    }

  /* Search for a large enough chunk in the list of nodes.
   * This list is ordered by size, but will have occasional
   * zero sized nodes as we visit other ldev->mm.nodelist[] entries.
   */

  for (node = ldev->mm.nodelist[ndx].flink;
       node && node->size < size;
       node = node->flink);

  /* If we found a node with non-zero size, then this is one
   * to use. Since the list is ordered, we know that is must be
   * best fitting chunk available.
   */

  if (node)
    {
      struct mm_freenode_s *remainder;
      struct mm_freenode_s *next;
      size_t remaining;

      /* Remove the node.  There must be a predecessor, but there may
       * not be a successor node.
       */

      //DEBUGASSERT(node->blink);
      node->blink->flink = node->flink;
      if (node->flink)
        {
          node->flink->blink = node->blink;
        }

      /* Check if we have to split the free node into one of the
       * allocated size and another smaller freenode.  In some
       * cases, the remaining bytes can be smaller (they may be
       * SIZEOF_MM_ALLOCNODE).  In that case, we will just carry
       * the few wasted bytes at the end of the allocation.
       */

      remaining = node->size - size;
      if (remaining >= SIZEOF_MM_FREENODE)
        {
          /* Get a pointer to the next node in physical memory */

          next = (struct mm_freenode_s*)(((char*)node) + node->size);

          /* Create the remainder node */

          remainder = (struct mm_freenode_s*)(((char*)node) + size);
          remainder->size = remaining;
          remainder->preceding = size;

          /* Adjust the size of the node under consideration */

          node->size = size;

          /* Adjust the 'preceding' size of the (old) next node,
           * preserving the allocated flag.
           */

          next->preceding = remaining | (next->preceding & MM_ALLOC_BIT);

          /* Add the remainder back into the nodelist */

          mm_addfreechunk(remainder);
        }

      /* Handle the case of an exact size match */

      node->preceding |= MM_ALLOC_BIT;
      ret = (void*)((char*)node + SIZEOF_MM_ALLOCNODE);
    }

#if (CONFIG_ROM_VER > 1)
	irq_restore(save);
#endif
  //mm_givesemaphore();
  //serial_printf("Allocated %p\n", ret);
#ifdef MEM_INFO_DEBUG 
  if(fun_add_mem_info)
  	(*fun_add_mem_info)(ret,ret_addr);
#endif
#ifdef MEM_WARNING
  if(!ret)
	serial_printf("MEM WARNING: malloc failure, ret_addr=%x\n",ret_addr);
#endif

#ifdef DEBUG_MEM_CORRUPT
  if(ret == memory_district)
  {
    serial_printf("Hit memory district!!, ret_addr=%x, size=%d\n",ret_addr, size);
  }
#endif	
  return ret;
}

/*----------------------------------------------------------------*/
/**
 * The free function causes the space pointed to by mem to be
 * deallocated, that is, made available for further allocation.
 *
 * @param mem Pointer to a previously allocated region of memory to be freed.
 * @return None.
 */
/*----------------------------------------------------------------*/
void free(void *mem)
{
	struct mm_freenode_s *node;
	struct mm_freenode_s *prev;
	struct mm_freenode_s *next;
	unsigned int save;
#ifdef MEM_WARNING
  int ret_addr = (int)__builtin_return_address(0);
#endif

  //mvdbg("Freeing %p\n", mem);

  /* Protect against attempts to free a NULL reference */

  if (!mem)
  {
#ifdef MEM_WARNING
		serial_printf("MEM WARNING: free zero pointer, ret_addr=%x\n",ret_addr);
#endif
      return;
  }
#ifdef MEM_INFO_DEBUG
  if(fun_remove_mem_info)
	(*fun_remove_mem_info)(mem);
#endif
  /* We need to hold the MM semaphore while we muck with the
   * nodelist.
   */
#if (CONFIG_ROM_VER > 1)
	save = irq_save();
#endif
  //mm_takesemaphore();

  /* Map the memory chunk into a free node */

  node = (struct mm_freenode_s *)((char*)mem - SIZEOF_MM_ALLOCNODE);
  node->preceding &= ~MM_ALLOC_BIT;

  /* Check if the following node is free and, if so, merge it */

  next = (struct mm_freenode_s *)((char*)node + node->size);
  if ((next->preceding & MM_ALLOC_BIT) == 0)
    {
      struct mm_allocnode_s *andbeyond;

      /* Get the node following the next node (which will
       * become the new next node). We know that we can never
       * index past the tail chunk because it is always allocated.
       */

      andbeyond = (struct mm_allocnode_s*)((char*)next + next->size);

      /* Remove the next node.  There must be a predecessor,
       * but there may not be a successor node.
       */

      //DEBUGASSERT(next->blink);
      next->blink->flink = next->flink;
      if (next->flink)
        {
          next->flink->blink = next->blink;
        }

      /* Then merge the two chunks */

      node->size          += next->size;
      andbeyond->preceding =  node->size | (andbeyond->preceding & MM_ALLOC_BIT);
      next                 = (struct mm_freenode_s *)andbeyond;
    }

  /* Check if the preceding node is also free and, if so, merge
   * it with this node
   */

  prev = (struct mm_freenode_s *)((char*)node - node->preceding);
  if ((prev->preceding & MM_ALLOC_BIT) == 0)
    {
      /* Remove the node.  There must be a predecessor, but there may
       * not be a successor node.
       */

      //DEBUGASSERT(prev->blink);
      prev->blink->flink = prev->flink;
      if (prev->flink)
        {
          prev->flink->blink = prev->blink;
        }

      /* Then merge the two chunks */

      prev->size     += node->size;
      next->preceding = prev->size | (next->preceding & MM_ALLOC_BIT);
      node            = prev;
    }

  /* Add the merged node to the nodelist */

  mm_addfreechunk(node);
#if (CONFIG_ROM_VER > 1)
	irq_restore(save);
#endif
  //mm_givesemaphore();
}	

/*----------------------------------------------------------------*/
/**
 * The function invokes free() to cause non zero space pointed to
 * by mem to be deallocated.
 *
 * @param mem Pointer to a previously allocated region of memory to be freed.
 * @return None.
 */
/*----------------------------------------------------------------*/
void free_nonzero(void *mem)
{
  if (mem)  
    free(mem);
}

void *x_malloc( size_t size )
{
	struct mm_freenode_s *node;
  void *ret = NULL;
  int ndx;
  unsigned int save;

  /* Handle bad sizes */

  if (size <= 0)
    {
      return NULL;
    }

  /* Adjust the size to account for (1) the size of the allocated
   * node and (2) to make sure that it is an even multiple of
   * our granule size.
   */

  size = MM_ALIGN_UP(size + SIZEOF_MM_ALLOCNODE);

  /* We need to hold the MM semaphore while we muck with the
   * nodelist.
   */
#if (CONFIG_ROM_VER > 1)
	save = irq_save();
#endif
  //mm_takesemaphore();

  /* Get the location in the node list to start the search.
   * Special case really big alloctions
   */

  if (size >= MM_MAX_CHUNK)
    {
      ndx = MM_NNODES-1;
    }
  else
    {
      /* Convert the request size into a nodelist index */

      ndx = mm_size2ndx(size);
    }

  /* Search for a large enough chunk in the list of nodes.
   * This list is ordered by size, but will have occasional
   * zero sized nodes as we visit other ldev->mm.nodelist[] entries.
   */

  for (node = ldev->mm.nodelist[ndx].flink;
       node && node->size < size;
       node = node->flink);

  /* If we found a node with non-zero size, then this is one
   * to use. Since the list is ordered, we know that is must be
   * best fitting chunk available.
   */

  if (node)
    {
      struct mm_freenode_s *remainder;
      struct mm_freenode_s *next;
      size_t remaining;

      /* Remove the node.  There must be a predecessor, but there may
       * not be a successor node.
       */

      //DEBUGASSERT(node->blink);
      node->blink->flink = node->flink;
      if (node->flink)
        {
          node->flink->blink = node->blink;
        }

      /* Check if we have to split the free node into one of the
       * allocated size and another smaller freenode.  In some
       * cases, the remaining bytes can be smaller (they may be
       * SIZEOF_MM_ALLOCNODE).  In that case, we will just carry
       * the few wasted bytes at the end of the allocation.
       */

      remaining = node->size - size;
      if (remaining >= SIZEOF_MM_FREENODE)
        {
          /* Get a pointer to the next node in physical memory */

          next = (struct mm_freenode_s*)(((char*)node) + node->size);
	  
          /* Create the remainder node */

          remainder = (struct mm_freenode_s*)node;
          remainder->size = remaining;

          /* Adjust the size of the node under consideration */
	  node = (struct mm_freenode_s *)((char *)next - size);
          node->size = size;
	  node->preceding = remaining;

          /* Adjust the 'preceding' size of the (old) next node,
           * preserving the allocated flag.
           */

          next->preceding = size | (next->preceding & MM_ALLOC_BIT);

          /* Add the remainder back into the nodelist */

          mm_addfreechunk(remainder);
        }

      /* Handle the case of an exact size match */

      node->preceding |= MM_ALLOC_BIT;
      ret = (void*)((char*)node + SIZEOF_MM_ALLOCNODE);
    }

#if (CONFIG_ROM_VER > 1)
	irq_restore(save);
#endif
  //mm_givesemaphore();
  //serial_printf("Allocated %p\n", ret);
  return ret;
}

/*----------------------------------------------------------------*/
/**
 * The zalloc function invokes malloc() to allocate space. The space
 * is initialized to all bits zero.
 *
 * @param size Size, in bytes, of the objects to allocate space for.
 * @return NULL is returned if the space could not be allocated.
 * Otherwise, a pointer to a region of the requested size is returned.
 */
/*----------------------------------------------------------------*/
void *zalloc(size_t size)
{
  void *alloc = malloc(size);
  if (alloc)
    {
       memset(alloc, 0, size);
    }

  return alloc;
}

/*----------------------------------------------------------------*/
/**
 * The calloc function allocates space for an array of Num objects, each of
 * whose size is Size. The space is initialized to all bits zero.
 *
 * @param n Number of objects to allocate.
 * @param size Size, in bytes, of the objects to allocate space for.
 * @return NULL is returned if the space could not be allocated.
 * Otherwise, a pointer to a region of the requested size is returned.
 */
/*----------------------------------------------------------------*/
void *calloc( size_t n, size_t size )
{
	void *ret = NULL;
	
	if (n > 0 && size > 0)
    {
      ret = zalloc(n * size);
    }

  return ret;
}

/*----------------------------------------------------------------*/
/**
 * The realloc function changes the size of the object pointed to by
 * oldmem to the size specified by size.
 *
 * @param oldmem Pointer to a previously allocated region of memory
 *  to be resized.
 * @param size Size, in bytes, of the new object to allocate space for.
 * @return NULL is returned if the space could not be allocated.
 * Otherwise, a pointer to a region of the requested size is returned.
 */
/*----------------------------------------------------------------*/
void *realloc(void *oldmem, size_t size)
{
	struct mm_allocnode_s *oldnode;
  struct mm_freenode_s  *prev;
  struct mm_freenode_s  *next;
  size_t oldsize;
  size_t prevsize = 0;
  size_t nextsize = 0;
  void *newmem;
  unsigned int save;

  /* If oldmem is NULL, then realloc is equivalent to malloc */

  if (!oldmem)
    {
      return malloc(size);
    }

  /* If size is zero, then realloc is equivalent to free */

  if (size <= 0)
    {
      free(oldmem);
      return NULL;
    }

  /* Adjust the size to account for (1) the size of the allocated
   * node and (2) to make sure that it is an even multiple of
   * our granule size.
   */

  size = MM_ALIGN_UP(size + SIZEOF_MM_ALLOCNODE);

  /* Map the memory chunk into an allocated node structure */

  oldnode = (struct mm_allocnode_s *)((char*)oldmem - SIZEOF_MM_ALLOCNODE);

  /* We need to hold the MM semaphore while we muck with the
   * nodelist.
   */
#if (CONFIG_ROM_VER > 1)
  save = irq_save();
#endif
  //mm_takesemaphore();
  /* Check if this is a request to reduce the size of the allocation. */

  oldsize = oldnode->size;
  if (size <= oldsize)
    {
      /* Handle the special case where we are not going to change the
       * size of the allocation.
       */
      if (size < oldsize)
        {
          mm_shrinkchunk(oldnode, size);
      }

      /* Then return the original address */
#if (CONFIG_ROM_VER > 1)
      irq_restore(save);
#endif
      //mm_givesemaphore();
      return oldmem;
    }

  /* This is a request to increase the size of the allocation,  Get the
   * available sizes before and after the oldnode so that we can make
   * the best decision
   */

  next = (struct mm_freenode_s *)((char*)oldnode + oldnode->size);
  if ((next->preceding & MM_ALLOC_BIT) == 0)
    {
      nextsize = next->size;
    }

  prev = (struct mm_freenode_s *)((char*)oldnode - (oldnode->preceding & ~MM_ALLOC_BIT));
  if ((prev->preceding & MM_ALLOC_BIT) == 0)
    {
      prevsize = prev->size;
    }

  /* Now, check if we can extend the current allocation or not */

  if (nextsize + prevsize + oldsize >= size)
    {
      size_t needed   = size - oldsize;
      size_t takeprev = 0;
      size_t takenext = 0;

      /* Check if we can extend into the previous chunk and if the
       * previous chunk is smaller than the next chunk.
       */

      if (prevsize > 0 && (nextsize >= prevsize || nextsize <= 0))
        {
           /* Can we get everything we need from the previous chunk? */

           if (needed > prevsize)
             {
               /* No, take the whole previous chunk and get the
                * rest that we need from the next chunk.
                */

               takeprev = prevsize;
               takenext = needed - prevsize;
             }
           else
             {
               /* Yes, take what we need from the previous chunk */

               takeprev = needed;
               takenext = 0;
             }

           needed = 0;
         }

      /* Check if we can extend into the next chunk and if we still
       * need more memory.
       */

      if (nextsize > 0 && needed)
        {
           /* Can we get everything we need from the next chunk? */

           if (needed > nextsize)
             {
               /* No, take the whole next chunk and get the
                * rest that we need from the previous chunk.
                */

               takeprev = needed - nextsize;
               takenext = nextsize;
             }
           else
             {
               /* Yes, take what we need from the previous chunk */

               takeprev = 0;
               takenext = needed;
             }
        }

      /* Extend into the previous free chunk */

      newmem = oldmem;
      if (takeprev)
        {
           struct mm_allocnode_s *newnode;

           /* Remove the previous node.  There must be a predecessor,
            * but there may not be a successor node.
            */

           //DEBUGASSERT(prev->blink);
           prev->blink->flink = prev->flink;
           if (prev->flink)
             {
               prev->flink->blink = prev->blink;
             }

           /* Extend the node into the previous free chunk */

           newnode = (struct mm_allocnode_s *)((char*)oldnode - takeprev);

           /* Did we consume the entire preceding chunk? */

           if (takeprev < prevsize)
             {
               /* No.. just take what we need from the previous chunk
                * and put it back into the free list
                */

               prev->size        -= takeprev;
               newnode->size      = oldsize + takeprev;
               newnode->preceding = prev->size | MM_ALLOC_BIT;
               next->preceding    = newnode->size | (next->preceding & MM_ALLOC_BIT);

               /* Return the previous free node to the nodelist (with the new size) */

               mm_addfreechunk(prev);

              /* Now we want to return newnode */

               oldnode = newnode;
             }
           else
             {
                /* Yes.. update its size (newnode->preceding is already set) */

               newnode->size      += oldsize;
               newnode->preceding |= MM_ALLOC_BIT;
               next->preceding     = newnode->size | (next->preceding & MM_ALLOC_BIT);
             }

           oldnode = newnode;
           oldsize = newnode->size;

          /* Now we have to move the user contents 'down' in memory.  memcpy should
           * should be save for this.
           */

          newmem = (void*)((char*)newnode + SIZEOF_MM_ALLOCNODE);
          memcpy(newmem, oldmem, oldsize - SIZEOF_MM_ALLOCNODE);
        }

      /* Extend into the next free chunk */

      if (takenext)
        {
          struct mm_freenode_s *newnode;
          struct mm_allocnode_s *andbeyond;

          /* Get the chunk following the next node (which could be the tail chunk) */

          andbeyond = (struct mm_allocnode_s*)((char*)next + nextsize);

          /* Remove the next node.  There must be a predecessor,
           * but there may not be a successor node.
           */

          //DEBUGASSERT(next->blink);
          next->blink->flink = next->flink;
          if (next->flink)
            {
              next->flink->blink = next->blink;
            }

          /* Extend the node into the next chunk */

          oldnode->size = oldsize + takenext;
          newnode       = (struct mm_freenode_s *)((char*)oldnode + oldnode->size);

          /* Did we consume the entire preceding chunk? */

          if (takenext < nextsize)
            {
              /* No, take what we need from the next chunk and return it
               * to the free nodelist.
               */

              newnode->size        = nextsize - takenext;
              newnode->preceding   = oldnode->size;
              andbeyond->preceding = newnode->size | (andbeyond->preceding & MM_ALLOC_BIT);

              /* Add the new free node to the nodelist (with the new size) */

              mm_addfreechunk(newnode);
            }
          else
            {
              /* Yes, just update some pointers. */

              andbeyond->preceding = oldnode->size | (andbeyond->preceding & MM_ALLOC_BIT);
            }
        }
#if (CONFIG_ROM_VER > 1)
		irq_restore(save);
#endif
      //mm_givesemaphore();
      return newmem;
    }

  /* The current chunk cannot be extended.  Just allocate a new chunk and copy */

  else
    {
       /* Allocate a new block.  On failure, realloc must return NULL but
        * leave the original memory in place.
        */
#if (CONFIG_ROM_VER > 2)
		irq_restore(save);
#elif (CONFIG_ROM_VER > 1)
		irq_enable();
#endif
       //mm_givesemaphore();
       newmem = (void*)malloc(size);
       if (newmem)
         {
           memcpy(newmem, oldmem, oldsize);
           free(oldmem);
         }

       return newmem;
    }
}

#if defined(CONFIG_CMD_MI)
int _mallinfo(int argc, char *argv[])
{
  struct mm_allocnode_s *node;
  size_t mxordblk = 0; 
  int    ordblks  = 0;  /* Number of non-inuse chunks */
  size_t uordblks = 0;  /* Total allocated space */
  size_t fordblks = 0;  /* Total non-inuse space */
#if CONFIG_MM_REGIONS > 1
  int region;
#else
# define region 0
#endif

  /* Visit each region */
#if CONFIG_MM_REGIONS > 1
  for (region = 0; region < g_nregions; region++)
#endif
    {
      /* Visit each node in the region */

      for (node = ldev->mm.heapstart[region];
           node < ldev->mm.heapend[region];
           node = (struct mm_allocnode_s *)((char*)node + node->size))
        {
          if (node->preceding & MM_ALLOC_BIT)
            {
              uordblks += node->size;
            }
          else
            {
              ordblks++;
              fordblks += node->size;
              if (node->size > mxordblk)
                {
                  mxordblk = node->size;
                }
            }
        }

      //DEBUGASSERT(node == ldev->mm.heapend[region]);
      uordblks += SIZEOF_MM_ALLOCNODE; /* account for the tail node */
    }
#undef region

  //DEBUGASSERT(uordblks + fordblks == ldev->mm.heapsize);
  char buf[128];
  sprintf(buf,"heapsize=%d, ordblks=%d, mxordblk=%d, uordblks=%d, fordblks=%d\n", ldev->mm.heapsize, ordblks, mxordblk, uordblks, fordblks);
	uart0_sendStr(buf);
  //serial_printf("heapsize=%d, ordblks=%d, mxordblk=%d, uordblks=%d, fordblks=%d\n", ldev->mm.heapsize, ordblks, mxordblk, uordblks, fordblks);
	return ERR_OK;
 }

cmdt cmdt_free __attribute__ ((section("cmdt"))) =
{"mi", _mallinfo, "minfo"};

#endif
