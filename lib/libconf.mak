libdirs-$(CONFIG_HTTPC)	+= httpc
#libdirs-$(CONFIG_LIBCOAP)	+= libcoap-dtls
libdirs-$(CONFIG_WOLFSSL)	+= wolfssl
libdirs-$(CONFIG_WRSA)		+= wrsa
#libdirs-$(CONFIG_AXTLS)		+= axtls
#libdirs-$(CONFIG_FREERTOS)	+= freertos
#libdirs-y				+= common
libdirs-y				+= sys
#libdirs-y				+= airkiss
libdirs-$(CONFIG_HTTPD)	+= http
libdirs-$(CONFIG_WEB_AS_DATA)	+= www
libdirs-$(CONFIG_WLA) 	+= wla
#libdirs-$(CONFIG_LWIP)	+= lwip
libdirs-$(CONFIG_DEV)	+= dev
#libdirs-y				+=axtls

